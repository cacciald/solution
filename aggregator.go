package main

import (
	"sort"
	"sync"
	"time"
)

const timeBetweenScans time.Duration = 5 * time.Second

type traceAccumulator struct {
	LastWrite   time.Time    // Last time an event was written to this structure
	ParsedLines []ParsedLine // All the parsed lines for this traceid
}

type inMemoryDataStore struct {
	Mutex  *sync.Mutex                 // Used for safe multithread access
	Traces map[string]traceAccumulator // store all the traces in memory
}

// Length function used later by sort.Sort()
func (ta *traceAccumulator) Len() int {
	return len(ta.ParsedLines)
}

// Function to identify order used by sort.Sort()
func (ta *traceAccumulator) Less(i, j int) bool {
	return ta.ParsedLines[i].StartingTS.Before(ta.ParsedLines[j].StartingTS)
}

// Swapping function used by sort.Sort()
func (ta *traceAccumulator) Swap(i, j int) {
	ta.ParsedLines[i], ta.ParsedLines[j] = ta.ParsedLines[j], ta.ParsedLines[i]
}

func (ta *traceAccumulator) Sort() {
	sort.Sort(ta)
}

// A function used to save the parsed line in an inMemory database.
func (db *inMemoryDataStore) save(pl *ParsedLine, wg *sync.WaitGroup) {
	defer wg.Done()
	// variable for parsed lines
	var pls []ParsedLine

	// using mutex for safe multithreaded access to structure
	db.Mutex.Lock()
	if _, ok := db.Traces[pl.TraceID]; ok {
		// If it exists we accumulate the new line
		pls = append(db.Traces[pl.TraceID].ParsedLines, *pl)
	} else {
		// If it doesn't we just initialize it
		pls = append([]ParsedLine{}, *pl)
	}
	// last we assign the new accumulator
	db.Traces[pl.TraceID] = traceAccumulator{
		LastWrite:   time.Now(),
		ParsedLines: pls,
	}
	// we release the lock
	db.Mutex.Unlock()
}

// This method will go over the data structure to
// process events older than 15 seconds
func (db *inMemoryDataStore) scanner() {
	secs := -15 * time.Second
	timeTreshhold := time.Now().Add(secs)
	// here we initialize a waiting group that will be used to wait for all the async tasks to be done
	var wg sync.WaitGroup

	// Aquire Mutes lock
	db.Mutex.Lock()
	for k := range db.Traces {
		// If the last event has happened more than 15 seconds ago
		if db.Traces[k].LastWrite.Before(timeTreshhold) {
			// Async process to free the db structure as fast as possible
			wg.Add(1)
			go processTrace(db.Traces[k], &wg)
			// Here we pop the entry that was already processed
			delete(db.Traces, k)
		}
	}
	db.Mutex.Unlock()
	// here we wait for things to finish
	wg.Wait()
}

// A function that scans the db for event every certain amount of time to clean the buffer
// Always throw this in a go routine
func (db *inMemoryDataStore) cronScanner() {
	for {
		time.Sleep(timeBetweenScans)
		db.scanner()
	}
}
