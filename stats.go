package main

import (
	"sync"
)

type stats struct {
	Mutex          *sync.Mutex
	MalformedLines int
	OrphanLines    int
	ProcessedLines int
	ValidTraces    int
}

// Thread safe counter
func (s *stats) incMalforfed() {
	s.Mutex.Lock()
	s.MalformedLines++
	s.Mutex.Unlock()
}

// Thread safe counter
func (s *stats) incOrphan() {
	s.Mutex.Lock()
	s.OrphanLines++
	s.Mutex.Unlock()
}

// Thread safe counter
func (s *stats) incProcessedLines() {
	s.Mutex.Lock()
	s.ProcessedLines++
	s.Mutex.Unlock()
}

// Thread safe counter
func (s *stats) incValidTraces() {
	s.Mutex.Lock()
	s.ValidTraces++
	s.Mutex.Unlock()
}
