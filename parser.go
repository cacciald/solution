package main

import (
	"errors"
	"strings"
	"time"
)

// ParsedLine is the base structure for everything
type ParsedLine struct {
	StartingTS, EndingTS                   time.Time
	TraceID, ServiceName, CallerSpan, Span string
}

// A function that decodes the line sent as
func parseLine(s string) (*ParsedLine, error) {
	pl := &ParsedLine{}
	fields := strings.Split(s, " ")
	if len(fields) != 5 {
		statsKeeper.incMalforfed()
		return pl, errors.New("Malformed Line")
	}
	startingTS, err := parseDate(&fields[0])
	if err != nil {
		statsKeeper.incMalforfed()
		return pl, err
	}
	endingTS, err := parseDate(&fields[1])
	if err != nil {
		statsKeeper.incMalforfed()
		return pl, err
	}
	callerSpan, span, err := parseSpans(&fields[4])
	if err != nil {
		statsKeeper.incMalforfed()
		return pl, err
	}

	pl = &ParsedLine{
		StartingTS:  startingTS,
		EndingTS:    endingTS,
		TraceID:     fields[2],
		ServiceName: fields[3],
		CallerSpan:  callerSpan,
		Span:        span,
	}

	return pl, nil
}

func parseDate(s *string) (time.Time, error) {
	time, err := time.Parse(time.RFC3339, *s)
	if err != nil {
		return time, err
	}
	return time, nil
}

func parseSpans(s *string) (callerSpan, span string, err error) {
	spans := strings.Split(*s, "->")
	if len(spans) != 2 {
		return "", "", errors.New("Malformed Spans")
	}

	// Here we replace null by root to avoid further issues
	if spans[0] == "null" {
		spans[0] = "root"
	}

	return spans[0], spans[1], nil
}
