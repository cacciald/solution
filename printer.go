package main

import (
	"errors"
	"fmt"
	"sync"
)

// node for tree structure
type node struct {
	id       string `json:"span"`
	service, start, end string
	children []*node `json:"calls"`
}

func processTrace(traces traceAccumulator, wg *sync.WaitGroup) {
	defer wg.Done()
	// First we sort the entries
	traces.Sort()
	if traces.ParsedLines[0].CallerSpan != "root" {
		statsKeeper.incOrphan()
		fmt.Println("Here we should count the error and exit")
	} else {
		root = buildBaseNode(traces.ParsedLines[0])

		// here we build the tree growing from root
		for i := range traces.ParsedLines {
			// We skip the root
			if i != 0 {
				// first we build our node
				node = buildBaseNode(traces.ParsedLines[i])
				parent = searchNodeByIDDFS(root, traces.ParsedLines[i].CallerSpan)
				parent.attachChild(node)
			}
		}
		statsKeeper.incValidTraces()
		fmt.Println("We process the shit")
	}
}

// serchNodeByIDDFS depth first search
// Not so safe given the lack of tail recursion optimization
// TODO: make this safe by implementing TRO
func serchNodeByIDDFS(n *node, id string) *node {
	if n.id == id {
		return n
	}

	if len(n.children) > 0 {
		for _, child := range n.children {
			serchNodeByIDDFS(child, id)
		}
	}
	return nil
}

func buildBaseNode(p *ParsedLine) *node {
	n := &node{
		id: p.Span,
		service: p.ServiceName,
		start: p.StartingTS.Format(time.RFC3339),
		end: p.EndingTS.Format(time.RFC3339),
		children: make([]*node, 0, 0)
	}
	return n
}

// function that attaches the child to the parent
func (parent *node) attachChild(child *node)  {
	parent.children = append(parent.children, child)
}