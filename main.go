package main

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

// Max lines processed before triggering a scan of the db for searching structures to process
const maxLinesProcessed int = 1000

var statsKeeper *stats = &stats{
	Mutex:          &sync.Mutex{},
	MalformedLines: 0,
	OrphanLines:    0,
}

func main() {
	// Buffered reader from stdin
	scanner := bufio.NewScanner(os.Stdin)
	// counter for amount of lines processed
	linesProccesed := 0

	// Here we initialize our database
	db := inMemoryDataStore{
		Mutex: &sync.Mutex{},
	}
	// Here we start our cron based "cleaner"
	go db.cronScanner()

	// here we initialize a waiting group that will be used to wait for all the async tasks to be done
	var wg sync.WaitGroup

	for scanner.Scan() {

		pl, err := parseLine(scanner.Text())
		// Here we increment the counter for lines processed
		linesProccesed++
		statsKeeper.incProcessedLines()

		if err != nil {
			// here we increment the counter for malformed lines and send the error to std err
			statsKeeper.incMalforfed()
			// don't care about waiting for this to print
			go fmt.Fprintf(os.Stderr, err.Error())
		} else {
			// Try to save the proccessed line async to don't block the main thread
			wg.Add(1)
			go db.save(pl, &wg)

			// To avoid having a large database in memory we force the scanner every 1000 lines processed
			if linesProccesed > maxLinesProcessed {
				// here we should scan our db!!!
				go db.scanner()
			}
		}
	}

	// Wait for all sync events to finish
	wg.Wait()

	if err := scanner.Err(); err != nil {
		fmt.Fprintf(os.Stderr, err.Error())
	}
}
